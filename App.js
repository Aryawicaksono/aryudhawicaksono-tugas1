import React, {useState} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

const App = () => {
  return (
    <View
      style={{
        backgroundColor: '#F7F7F7',
        paddingHorizontal: 20,
        paddingTop: 199,
        paddingBottom: 180,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <View
        style={{
          height: 381,
          backgroundColor: '#FFFFFF',
          borderRadius: 20,
          alignItems: 'center',
        }}>
        <View
          style={{
            marginHorizontal: 75,
            height: 36,
            minWidth: 170,
            backgroundColor: '#002558',
            borderRadius: 96,
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: -20,
            marginHorizontal: 95,
          }}>
          <Text
            style={{
              color: '#FFFFFF',
              fontWeight: 'bold',
              fontSize: 16,
            }}>
            Digital Approval
          </Text>
        </View>
        <Image
          style={{
            marginTop: 25,
            height: 55,
            width: 127,
          }}
          source={require('./gambar.jpg')}
        />
        <TextInput
          placeholder="Alamat Email"
          style={{
            borderWidth: 0.5,
            borderRadius: 5,
            height: 48,
            width: 288,
            marginTop: 38,
            marginHorizontal: 10,
            borderColor: 'black',
            fontSize: 14,
            paddingLeft: 44,
          }}
        />
        <TextInput
          placeholder="Password"
          style={{
            borderWidth: 0.5,
            borderRadius: 5,
            height: 48,
            width: 288,
            marginTop: 20,
            marginHorizontal: 10,
            borderColor: 'black',
            fontSize: 14,
            paddingLeft: 44,
          }}
        />
        <Text
          style={{
            marginTop: 16,
            marginLeft: 201,
            marginRight: 10,
            fontStyle: 'italic',
          }}>
          Reset Password
        </Text>
        <TouchableOpacity
          style={{
            height: 48,
            width: 288,
            marginTop: 24,
            borderRadius: 5,
            backgroundColor: '#287AE5',
            paddingTop: 13,
            paddingBottom: 14,
            paddingHorizontal: 120,
          }}>
          <Text
            style={{
              color: 'white',
              fontSize: 16,
              fontWeight: 'bold',
            }}>
            Login
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    backgroundColor: 'blue',
  },
});
export default App;
